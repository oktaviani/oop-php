<?php
require_once ('animal.php');
require_once ('frog.php');
require_once ('ape.php');

$sheep = new Animal("shaun");

echo $sheep->name; // "shaun"
echo "<br>";
echo $sheep->legs; // 2
echo "<br>";
echo $sheep->cold_blooded; // false
echo "<br>";

$sungokong = new Ape("kera sakti");

echo $sungokong->name;
echo "<br>";
echo $sungokong->legs;
echo "<br>";
echo $sungokong->yell(); // "Auooo"
echo "<br>";

$kodok = new Frog("buduk");

echo $kodok->name;
echo "<br>";
echo $kodok->legs;
echo "<br>";
$kodok->jump(); // "hop hop"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())